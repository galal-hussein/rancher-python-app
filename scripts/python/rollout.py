import cattle
import argparse
import logging
import urllib3


urllib3.disable_warnings()
logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)
log.setLevel(logging.INFO)

def rollout(args):
    log.info('Starting rolling the application on production..')
    # get client and p_client
    client, p_client = get_clients(args.apiurl, args.token, args.project)
    # refresh catalog
    refresh_catalog(client, args.catalog)
    # upgrade app
    upgrade_app(p_client, args.app, args.version)
    log.info('Application [' + args.app + '] upgraded successfully to version: ' + args.version)


def get_clients(api_url, token, project_name):
    log.info("Initializing new cluster client and project client")
    client = cattle.Client(url=api_url,token=token,verify=False)
    project = client.list_project(name=project_name)[0]
    p_url = project.links['self'] + '/schemas'
    p_client = cattle.Client(url=p_url,token=token,verify=False)
    return client, p_client

def upgrade_app(p_client, app_name, new_version):
    log.info("Getting app: " + app_name)
    app = p_client.list_app(name=app_name)[0]
    old_external_id = app.externalId
    new_external_id = get_new_external_id(old_external_id, new_version)
    app.upgrade(externalId=new_external_id)


def get_new_external_id(old_external_id, new_version):
    log.info("Getting new external ID")
    version_list = old_external_id.split("version=")
    log.info("Current deployed version: " + version_list[1])
    new_external_id = version_list[0] + "version="
    new_external_id += new_version
    log.info("New external id for catalog app: " + new_external_id)
    return new_external_id


def refresh_catalog(client, catalog_name):
    log.info("Refresh catalog " + catalog_name)
    log.info("Waiting for 30 seconds to pull updates")
    catalog = client.list_catalog(name=catalog_name)
    catalog.refresh()
    log.info("Catalog refreshed with new versions")

if __name__== "__main__":
  parser = argparse.ArgumentParser()
  subparsers = parser.add_subparsers()

  upgrade_parser = subparsers.add_parser('upgrade')
  upgrade_parser.add_argument('--apiurl')
  upgrade_parser.add_argument('--token')
  upgrade_parser.add_argument('--project')
  upgrade_parser.add_argument('--catalog')
  upgrade_parser.add_argument('--app')
  upgrade_parser.add_argument('--version')
  upgrade_parser.set_defaults(func=rollout)

  args = parser.parse_args()
  args.func(args)

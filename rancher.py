from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    return "<center><h1>Welcome from Rancher! this is version v0.1.1</h1></center>"

if __name__ == "__main__":
    app.run(host='0.0.0.0')
